package com.penkinio.orderservice.controller;

import com.penkinio.orderservice.dto.OrderRequest;
import com.penkinio.orderservice.service.OrderService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @CircuitBreaker(name = "inventory",fallbackMethod = "fallbackMethod")
    //@TimeLimiter(name = "inventory")
    @Retry(name = "inventory",fallbackMethod = "fallbackMethodRetry")
    public CompletableFuture<String> placeOrder(@RequestBody OrderRequest orderRequest){
        return CompletableFuture.supplyAsync(()->orderService.placeOrder(orderRequest));
    }

    public CompletableFuture<String> fallbackMethod(OrderRequest orderRequest,RuntimeException e){
        return CompletableFuture.supplyAsync(()->"Order Service is Down , Please try again later");
    }

    public CompletableFuture<String> fallbackMethodRetry(OrderRequest orderRequest,RuntimeException e){
        return CompletableFuture.supplyAsync(()->"Order Service is Down , you have max three attempts to try again later");
    }
}
