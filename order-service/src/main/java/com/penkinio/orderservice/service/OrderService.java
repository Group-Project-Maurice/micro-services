package com.penkinio.orderservice.service;

import com.penkinio.orderservice.dto.OrderRequest;

public interface OrderService {
    public String placeOrder(OrderRequest orderRequest);
}
