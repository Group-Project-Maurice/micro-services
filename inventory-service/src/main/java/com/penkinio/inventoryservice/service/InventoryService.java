package com.penkinio.inventoryservice.service;

import com.penkinio.inventoryservice.dto.InventoryResponse;

import java.util.List;

public interface InventoryService {
    public List<InventoryResponse> isInStock(List<String> skuCode) throws InterruptedException;
}
