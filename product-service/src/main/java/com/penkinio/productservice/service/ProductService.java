package com.penkinio.productservice.service;

import com.penkinio.productservice.dto.ProductRequest;
import com.penkinio.productservice.dto.ProductResponse;

import java.util.List;

public interface ProductService {
    public void createProduct(ProductRequest productRequest);

    public void deleteProduct(Long id);

    public void updateProduct(Long id, ProductRequest productRequest);

    public void getProduct(Long id);

    public List<ProductResponse> getAllProducts();
/*
    public void createProduct(ProductRequest productRequest) {
        Product product = new Product();
        product.setName(productRequest.getName());
        product.setPrice(productRequest.getPrice());
        product.setQuantity(productRequest.getQuantity());
        productRepository.save(product);
    }*/
}
